package net.cydhra.vibrant.api.enchantment

import net.cydhra.vibrant.api.item.VibrantItem

interface VibrantEnchantment {
    val effectIdentifier: Int
    val minimumLevel: Int
    val maximumLevel: Int
    val weight: Int

    fun canEnchantItem(item: VibrantItem): Boolean

    fun getMinEnchantmentThreshold(enchantmentLevel: Int): Int

    fun getMaxEnchantmentThreshold(enchantmentLevel: Int): Int

    fun isCompatible(other: VibrantEnchantment): Boolean

    fun getDisplayName(level: Int): String
}