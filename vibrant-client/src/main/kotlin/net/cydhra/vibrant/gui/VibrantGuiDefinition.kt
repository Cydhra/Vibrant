package net.cydhra.vibrant.gui

import net.cydhra.vibrant.VibrantClient.logger
import net.cydhra.vibrant.configuration.ConfigurationService
import java.io.File
import java.net.JarURLConnection
import java.net.URL
import java.nio.file.Files
import java.nio.file.StandardCopyOption

/**
 * A vibrant GUI definition modelled in a definition file (serialized as JSON within the client resources. There all required information to
 * use the GUI are defined
 */
class VibrantGuiDefinition(private val file: String, val keys: Array<Int>, private val includePaths: Array<String>) {

    /**
     * A URL pointing to the HTML file modelling the GUI
     */
    lateinit var interfaceUrl: URL
        private set

    /**
     * Initialize the GUI after loading. This cannot be done while creating the object, since the object is deserialized from JSON and
     * therefore [file] is not available in the constructor. In Fact, gson probably does not even use the constructor but borrows one from
     * [Object] using java Unsafe. Also, delegating to [lazy] does not work, since gson touches the lazily initialized fields too early.
     */
    fun init() {
        val target = File(ConfigurationService.settingsFolder, file)

        this.unpackFile(VibrantGuiDefinition::class.java.getResource(file), target)
        for (includedPath in includePaths) {
            this.unpackFile(VibrantGuiDefinition::class.java.getResource(includedPath),
                    File(ConfigurationService.settingsFolder, includedPath)
            )
        }

        this.interfaceUrl = target.toURI().toURL()
    }

    /**
     * Unpack the resource contents given by parameters. Decide whether the executable is a JAR or just a set of class files and copy the
     * contents respectively.
     *
     * @param resourceURL the URL of the resource file inside the application
     * @param unpackedFile the file where to copy the resource to
     */
    private fun unpackFile(resourceURL: URL, unpackedFile: File) {
        if (resourceURL.protocol == "jar") {
            val connection: JarURLConnection = resourceURL.openConnection() as JarURLConnection
            logger.debug("unpacking jar entry ${connection.jarEntry}")

            // recursively copy directories
            if (connection.jarEntry.isDirectory) {
                connection.jarFile.entries().toList()
                        .stream()
                        .filter { it.name.startsWith(connection.jarEntry.name) && it.name != connection.jarEntry.name }
                        .forEach {
                            unpackFile(URL(resourceURL.toExternalForm() + it.name.removePrefix(connection.jarEntry.name)),
                                    File(unpackedFile, it.name.removePrefix(connection.jarEntry.name))
                            )
                        }
            } else {
                (resourceURL.openConnection() as JarURLConnection).inputStream.use {
                    Files.copy(it, unpackedFile.also { it.parentFile.mkdirs() }.absoluteFile.toPath(), StandardCopyOption.REPLACE_EXISTING)
                }
            }

        } else {
            logger.debug("unpacking resource path ${resourceURL.toURI()}")
            File(resourceURL.toURI()).copyRecursively(unpackedFile.also { it.parentFile.mkdirs() }, true)
        }
    }
}