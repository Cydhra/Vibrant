package net.cydhra.vibrant.command.commands

import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.VibrantClient.logger
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback

/**
 * Echos the given argument string back into the callback.
 */
object EchoCommand : CommandHandler<EchoArguments>(userCommand = true) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: EchoArguments, callback: CefQueryCallback?) {
        callback?.success(arguments.text.joinToString(" ")) ?: logger.error("Echo command invoked without callback function.")
    }
}

class EchoArguments(parser: ArgParser) {
    val text by parser.positionalList("TEXT", "the text that shall be printed")
}