package net.cydhra.vibrant.command.commands

import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback

object DeleteWorldCommand : CommandHandler<DeleteWorldArguments>(userCommand = false) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: DeleteWorldArguments, callback: CefQueryCallback?) {
        callback?.success(if (VibrantClient.minecraft.getSaveLoaderInstance().deleteDirectory(arguments.folder)) "true" else "false")
    }

}

class DeleteWorldArguments(parser: ArgParser) {
    val folder by parser.positional("FOLDER", help = "name of the save folder to delete")
}