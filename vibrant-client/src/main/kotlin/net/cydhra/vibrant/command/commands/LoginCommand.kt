package net.cydhra.vibrant.command.commands

import com.google.gson.Gson
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import net.cydhra.nidhogg.YggdrasilClient
import net.cydhra.nidhogg.data.AccountCredentials
import net.cydhra.nidhogg.data.Session
import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback
import java.util.*

/**
 * Generate a session out of login data
 */
object LoginCommand : CommandHandler<LoginCommandArguments>(userCommand = false) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: LoginCommandArguments, callback: CefQueryCallback?) {
        val session = if (arguments.offline) {
            if (arguments.username == null) {
                callback?.failure(-1, "On offline sessions a username must be given")
                return
            }
            Session(UUID.randomUUID().toString(), arguments.username!!, "", arguments.clientToken)
        } else {
            if (arguments.password == null) {
                callback?.failure(-2, "On online sessions a password must be given")
                return
            }

            val accountCredentials = if (arguments.email != null)
                AccountCredentials(arguments.email!!, arguments.password!!)
            else if (arguments.username != null)
                AccountCredentials(arguments.username!!, arguments.password!!)
            else {
                callback?.failure(-1, "Either username or email must be given")
                return
            }

            YggdrasilClient(nidhoggClientToken = arguments.clientToken).login(accountCredentials)
        }

        callback?.success(Gson().toJson(session))
    }

}

class LoginCommandArguments(parser: ArgParser) {
    val username by parser.storing("-u", "--username", help = "the username of a non-migrated Mojang account").default<String?>(null)
    val password by parser.storing("-p", "--password", help = "the password of a non-migrated Mojang account").default<String?>(null)
    val email by parser.storing("-m", "--email", help = "the email of a migrated Mojang account").default<String?>(null)
    val clientToken by parser.storing("-t", "--token", help = "the client token used for refreshing the session later")
            .default(VibrantClient.AUTH_TOKEN)
    val offline by parser.flagging("-o",
            "--offline",
            help = "if set, no login at Mojang will be performed and an offline session will be generated"
    )
}
