package net.cydhra.vibrant.configuration

import com.uchuhimo.konf.ConfigSpec

/**
 * Proxy for module config specs to hierarchically place them inside the modules spec
 */
open class ModuleConfigurationSpec(moduleName: String) : ConfigSpec("modules.$moduleName")