package net.cydhra.vibrant.util.inventory

import net.cydhra.vibrant.VibrantClient.factory
import net.cydhra.vibrant.VibrantClient.minecraft
import net.cydhra.vibrant.api.inventory.VibrantPlayerInventory
import net.cydhra.vibrant.api.item.VibrantItem
import net.cydhra.vibrant.api.network.ClickType
import kotlin.math.min

object InventoryManager {

    const val PROTOCOL_MAX_ARMOR_SLOT = 8
    const val PROTOCOL_MAX_HOT_BAR_SLOT = 8

    fun statelesslySwapItemWithHotBar(sourceSlotId: Int, targetHotBarId: Int) {
        minecraft.thePlayer!!.openContainer.clickSlot(
                slotId = sourceSlotId,
                clickedButton = targetHotBarId,
                mode = ClickType.SWAP,
                player = minecraft.thePlayer!!
        )
        minecraft.playerController!!.netHandler.sendPacket(factory.newWindowClickPacket(
                windowId = 0,
                slotId = sourceSlotId,
                clickType = ClickType.SWAP,
                mouseButton = targetHotBarId,
                actionNumber = minecraft.thePlayer!!.openContainer.nextTransactionID(minecraft.thePlayer!!.playerInventory),
                clickedItem = null
        )
        )
    }

    /**
     * Search an item type in player's hot bar
     *
     * @return searched item's hot bar index
     */
    inline fun <reified T : VibrantItem> searchItemInHotBar(startIndex: Int = 0): Int {
        for ((index, stack) in minecraft.thePlayer!!.playerInventory
                .filter { (index, _) -> index <= PROTOCOL_MAX_HOT_BAR_SLOT }) {
            if (index < startIndex) continue
            if (stack?.getItem() ?: continue is T) {
                return index
            }
        }

        return -1
    }

    /**
     * Search an item type in given inventory and return its slot index
     *
     * @param inventory the inventory to search
     *
     * @return searched item's index
     */
    inline fun <reified T : VibrantItem> searchItemInInventory(inventory: VibrantPlayerInventory): Int {
        for ((index, stack) in inventory) {
            if (stack?.getItem() ?: continue is T) {
                return index
            }
        }

        return -1
    }

    /**
     * Select an item in the hot bar
     *
     * @param hotBarIndex index in hot bar to select
     */
    fun selectItem(hotBarIndex: Int) {
        minecraft.thePlayer!!.playerInventory.currentSelectedIndex = hotBarIndex
    }
}