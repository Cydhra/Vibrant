package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantDynamicTexture;
import net.minecraft.client.renderer.texture.DynamicTexture;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(DynamicTexture.class)
public abstract class DynamicTextureMixin implements VibrantDynamicTexture {
    
}
