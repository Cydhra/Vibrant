package net.cydhra.vibrant.mixin.hook;

import net.cydhra.eventsystem.EventManager;
import net.cydhra.vibrant.events.render.RenderWorldEvent;
import net.minecraft.client.renderer.EntityRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(EntityRenderer.class)
public abstract class RenderWorldMixin {

    @Inject(method = "renderHand", at = @At("HEAD"))
    protected void onRenderHand(float partialTicks, int xOffset, final CallbackInfo info) {
        EventManager.callEvent(new RenderWorldEvent(partialTicks));
    }
}
