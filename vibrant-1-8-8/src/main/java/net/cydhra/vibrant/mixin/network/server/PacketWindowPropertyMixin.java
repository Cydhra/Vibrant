package net.cydhra.vibrant.mixin.network.server;

import net.cydhra.vibrant.api.network.server.VibrantPacketWindowProperty;
import net.minecraft.network.play.server.S31PacketWindowProperty;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(S31PacketWindowProperty.class)
public class PacketWindowPropertyMixin implements VibrantPacketWindowProperty {

    @Shadow
    private int windowId;
    @Shadow
    private int varIndex;
    @Shadow
    private int varValue;

    @Override
    public int getWindowIdentifier() {
        return windowId;
    }

    @Override
    public int getVariableIndex() {
        return varIndex;
    }

    @Override
    public int getValue() {
        return varValue;
    }
}
