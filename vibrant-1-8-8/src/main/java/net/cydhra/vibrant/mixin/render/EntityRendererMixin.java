package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantEntityRenderer;
import net.minecraft.client.renderer.EntityRenderer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(EntityRenderer.class)
public abstract class EntityRendererMixin implements VibrantEntityRenderer {

    @Shadow
    private int frameCount;

    @Shadow
    private boolean lightmapUpdateNeeded;

    @Shadow
    abstract void setupCameraTransform(float partialTicks, int pass);

    @Shadow
    abstract void updateLightmap(float partialTicks);

    @Override
    public int getFrameCount() {
        return this.frameCount;
    }

    @Override
    public void setFrameCount(int frameCount) {
        this.frameCount = frameCount;
    }

    @Override
    public void setupCameraTransform(float partialTicks) {
        this.setupCameraTransform(partialTicks, 2);
    }

    @Override
    public void doLightmapUpdate(float partialTicks) {
        this.updateLightmap(partialTicks);
    }

    @Override
    public boolean getLightmapUpdateNeeded() {
        return this.lightmapUpdateNeeded;
    }

    @Override
    public void setLightmapUpdateNeeded(boolean lightmapUpdateNeeded) {
        this.lightmapUpdateNeeded = lightmapUpdateNeeded;
    }
}
