package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantVertexFormat;
import net.minecraft.client.renderer.vertex.VertexFormat;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(VertexFormat.class)
public abstract class VertexFormatMixin implements VibrantVertexFormat {
}
