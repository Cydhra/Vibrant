package net.cydhra.vibrant.mixin.util.chat;

import net.cydhra.vibrant.api.util.chat.VibrantChatComponentText;
import net.minecraft.util.ChatComponentText;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(ChatComponentText.class)
public abstract class ChatComponentTextMixin implements VibrantChatComponentText {
}
