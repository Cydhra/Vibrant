package net.cydhra.vibrant.mixin.item;

import net.cydhra.vibrant.api.item.VibrantItemBow;
import net.minecraft.item.ItemBow;
import org.spongepowered.asm.mixin.Mixin;

@Mixin(ItemBow.class)
public abstract class ItemBowMixin implements VibrantItemBow {
}
