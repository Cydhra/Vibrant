package net.cydhra.vibrant.interfaces.item;

import net.cydhra.vibrant.api.world.VibrantBlockInfo;
import net.minecraft.item.ItemBlock;

public privileged aspect ItemBlockInterface {

    declare parents: net.minecraft.item.ItemBlock implements net.cydhra.vibrant.api.item.VibrantItemBlock;

    public VibrantBlockInfo ItemBlock.getBlockInfo() {
        return (VibrantBlockInfo) this.block;
    }
}
